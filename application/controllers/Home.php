<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
    {        
    	parent::__construct();

		$this->load->library('Api', 'api');
    }


	public function index()
	{

		try {

			$result = $this->api->getCategories();
			$categories = $result['categories'];

			

			$this->load->view('header');
			$this->load->view('home', compact('categories'));
			$this->load->view('footer');

		} catch (Exception $e) {
			echo $e->getMessage();
		}

		
	}

	public function view()
	{


		$category_name = str_replace(' ', '', $_GET['category_name']);

		try {

			$result = $this->api->getPlaces($category_name);

			if(!$result)
				show_404();

			$displayString = $result['geocode']['displayString'];
			$venues = $result['groups'];


			$this->load->view('header');
			$this->load->view('view', compact('venues', 'displayString'));
			
			$this->load->view('footer');

		} catch (Exception $e) {
			echo $e->getMessage();
		}
	
	}

	public function pr($value)
	{
		echo '<pre>';
		print_r($value);
		exit();
	}

}
