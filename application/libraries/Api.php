<?php

class Api
{
	private $base_url = 'https://api.foursquare.com/v2/';
	private $userless = 'client_id=GUOA1B0LEZL5HMLICZ3KMVHEPGMCJLKADNQA540AX2ROBGLH&client_secret=S3L25QHTLLMCWCMLW2COVN1X0ZCLO1FZCZ0GDO0YWCGCYJZX&v=20200101';

    public function getCategories($data = [])
    {

    	$url = $this->base_url . "venues/categories?" . $this->userless;

        return $this->apiRequest($url, 'GET', $data);
    }

    public function getPlaces($category_name)
    {

    	$url = $this->base_url . "venues/explore?near=valetta&query=" . $category_name. "&" . $this->userless;

        return $this->apiRequest($url, 'GET');
    }

    private function apiRequest($url, $method = 'POST', $inputs = [])
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);

        if($method == 'POST') {
        	curl_setopt($ch, CURLOPT_POST, 1);
        	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($inputs));
        }
     
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec ($ch);
		$error = curl_error($ch);

        curl_close($ch);


        $data = (Array) json_decode($server_output, true);

        if ($data['meta']['code'] != 200) {
            throw new Exception($data['meta']['errorDetail']);
        }

        return $data['response'];
    }
}