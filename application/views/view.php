<div class="container-fluid p-5">
	<h1><?= $displayString; ?></h1>
	<ul class="list-group list-inline">
		<?php foreach ($venues as $venue_items): ?>
			<?php foreach ($venue_items['items'] as $item): ?>
				<li class="list-group-item d-flex align-items-center bg-transparent mt-2" style="border: 2px solid">
					<img width="100" height="100" src="<?= base_url('assets/fs.png') ?>">
					<div class="ml-4">
						<p><b><?= $item['venue']['name'] ?></b></p>
						<p><?= $item['venue']['location']['formattedAddress'][0] .', '. $item['venue']['location']['formattedAddress'][1] . ', ' . $item['venue']['location']['formattedAddress'][2]  ?> </p>
					</div>

						
				</li>
			<?php endforeach ?>
		<?php endforeach ?>
	</ul>
</div>