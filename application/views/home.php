<div class="container home-container">
	<div class="row">
		<?php foreach ($categories as $key => $category): ?>
			<?php $url = urlencode($category['name']) ?>
			<a href="<?= base_url('home/view?category_name='). $url ?>" class="category-box">
				<img src="<?= $category['icon']['prefix'] .'64' . $category['icon']['suffix'] ?>" />
				<p class="category-title"><?= $category['shortName'] ?></p>
			</a>
		<?php endforeach ?>
	</div>
</div>

<style type="text/css">



</style>